import os
from Semantica import get_sense_for_target_word_and_context
from nltk.corpus import wordnet as wn

directory_pos = "hard/"
target_word = "hard"
window_size = 2

got_them_right = 0
total_examples = 0

for f in os.listdir(directory_pos):
    filename = os.fsdecode(f)

    if filename.startswith("README") or filename.startswith("."):
        continue

    file = open(os.path.join(directory_pos, filename))
    max_lines_per_file = 20
    phrase = file.readline()
    while phrase and max_lines_per_file > 0:
        synset = get_sense_for_target_word_and_context(target_word, phrase, window_size)
        if synset is not None:
            definition = synset.definition()

            # if filename.startswith('serve a purpose, role,'):
            #     if definition.startswith('serve a purpose, role,'):
            #         got_them_right += 1
            # elif filename.startswith('work for or be a servant'):
            #     if definition.startswith('work for or be a servant'):
            #         got_them_right += 1
            # elif filename.startswith('provide (usually but not '):
            #     if definition.startswith('provide (usually but not '):
            #         got_them_right += 1
            # elif filename.startswith('do duty or hold offices'):
            #     if definition.startswith('do duty or hold offices'):
            #         got_them_right += 1

            if filename.startswith('difficult'):
                if definition.startswith('not easy;'):
                    got_them_right += 1
            elif filename.startswith('requiring a great deal'):
                if definition.startswith('characterized by effort to the point'):
                    got_them_right += 1
            elif filename.startswith('solid'):
                if definition.startswith('very strong or') or definition.startswith("resisting weight or"):
                    got_them_right += 1

            # if filename.startswith("cord"):
            #     if definition.startswith('a conductor for transmitting electrical') or definition.startswith('something (as a cord or rope) that is'):
            #         got_them_right += 1
            # elif filename.startswith("division"):
            #     if definition.startswith('a conceptual separation or'):
            #         got_them_right += 1
            # elif filename.startswith("formation"):
            #     if definition.startswith('a formation of people or things'):
            #         got_them_right += 1
            # elif filename.startswith("phone"):
            #     if definition.startswith('a telephone'):
            #         got_them_right += 1
            # elif filename.startswith("product"):
            #     if definition.startswith('a particular kind of product'):
            #         got_them_right += 1
            # elif filename.startswith("text"):
            #     if definition.startswith('text consisting of a row of words'):
            #         got_them_right += 1

            max_lines_per_file -= 1
            total_examples += 1
            print("done example no. " + str(total_examples))
            print("expected: " + filename)
            print("got: " + definition)
            print("accuracy so far = " + str(got_them_right / total_examples))
            print()

        phrase = file.readline()

    file.close()

print("accuracy = " + str(got_them_right / total_examples))
