from Semantica import get_sense_for_target_word_and_context

file_test = open("test.txt", "r", encoding="utf-8")

phrase = file_test.readline()
target_word = file_test.readline().strip()
window_size = int(file_test.readline())

synset = get_sense_for_target_word_and_context(target_word, phrase, window_size)
print(synset.definition())
print(synset.name())
print(synset.lexname())
