from WNplusplus import synsets
from WNplusplus import wn_plus_plus

# systs = synsets('line')
# # definitions = [s.definition() for s in systs]
# # print(definitions)
mapping = wn_plus_plus('line', 'n')
print([(key, mapping[key].definition()) for key in mapping])
