import wikipedia
from wikipedia import DisambiguationError
from nltk.corpus import wordnet as wn
from nltk.stem import PorterStemmer
import numpy as np


def wn_plus_plus(word, part_of_speech):

    niu = {}

    # wikipages
    try:
        senses_wiki = [wikipedia.page(word)]
    except DisambiguationError as e:
        senses_wiki = []
        for option in e.options:
            try:
                sense = wikipedia.page(option)
                senses_wiki.append(sense)
            except DisambiguationError:
                pass

    # synsets from wordNet
    senses_wn = wn.synsets(word, pos=part_of_speech)

    for w in senses_wiki:
        niu[w.title] = None

    if len(senses_wiki) == len(senses_wn) == 1:
        niu[senses_wiki[0].title] = senses_wn[0]
    # for w in senses_wiki:
    #     if niu[w] == "epsilon":
    #         for d in senses_wiki:
    #             if w in wikipedia.page(d).links:
    #                 if niu[d] != "epsilon":
    #                     for sens_w in senses_wn:
    #                         if sens_w in wn.synset(niu[d].name()):
    #                             niu[w] = sens_w

    scor_general = 0
    for w in senses_wiki:
        for s in senses_wn:
            scor_general += compute_score(context_wiki(w), compute_synset_context(s))

    for w in senses_wiki:
        if niu[w.title] is None:
            vec = []
            for s in senses_wn:
                vec.append(compute_score(context_wiki(w), compute_synset_context(s)) / scor_general)
            niu[w.title] = senses_wn[np.argmax(np.array(vec))]

    return niu


def context_wiki(w):

    porter = PorterStemmer()
    context = []
    words = w.title.split()
    context += words
    links = w.links
    for l in links:
        words = l.split()
        context += words
    categories = w.categories
    for c in categories:
        words = c.split()
        for idx in range(0, len(words)):
            words[idx] = porter.stem(words[idx])
        context += words
    context = list(set(context))
    return context


def compute_synset_context(synset):
    context = []

    hypernyms = synset.hypernyms()
    hyponyms = synset.hyponyms()
    meronyms = synset.part_meronyms()
    holonyms = synset.part_holonyms()

    sisters = []
    for hyper in hypernyms:
        sis = hyper.hyponyms()
        for hypo in sis:
            sisters.append(hypo)

    for hyper in hypernyms:
        for lemma in hyper.lemmas():
            context.append(lemma.name())

    for hypo in hyponyms:
        for lemma in hypo.lemmas():
            context.append(lemma.name())

    for mero in meronyms:
        for lemma in mero.lemmas():
            context.append(lemma.name())

    for holo in holonyms:
        for lemma in holo.lemmas():
            context.append(lemma.name())

    for sis in sisters:
        for lemma in sis.lemmas():
            context.append(lemma.name())

    for lemma in synset.lemmas():
        context.append(lemma.name())

    context = list(set(context))
    return context


def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def compute_score(context_1, context_2):
    return len(intersection(context_1, context_2)) + 1


def synsets(word, pos='n'):
    syts = []
    dict = wn_plus_plus(word, pos)
    for key in dict:
        syts.append(dict[key])
    return syts
